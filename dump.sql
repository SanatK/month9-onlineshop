-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: artstore
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `artists`
--

DROP TABLE IF EXISTS `artists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `artists` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `surname` varchar(128) NOT NULL,
  `age` int NOT NULL,
  `description` varchar(255) NOT NULL,
  `photo` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `artists`
--

LOCK TABLES `artists` WRITE;
/*!40000 ALTER TABLE `artists` DISABLE KEYS */;
INSERT INTO `artists` VALUES (1,'Banksy','B',33,'The British artist, political activist','Banksy'),(2,'Darryl \"CornBread\"','McCray',54,'Born Darryl McCray, Cornbread is generally acknowledged to be the first modern graffiti artist','Darryl-CornBread'),(3,'Jean-Michel','Basquiat',41,'Among the most famous contemporary artists of all time','Basquiat-Jean-Michel'),(4,'Shepard','Fairey',48,'In 1989, a skateboarding enthusiast and Rhode Island School of Design student named Shepard Fairey started to post stickers featuring the face of the famed professional wrestler.','Shepard-Fairey');
/*!40000 ALTER TABLE `artists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `artworks`
--

DROP TABLE IF EXISTS `artworks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `artworks` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `category` varchar(128) NOT NULL DEFAULT 'artwork',
  `image` varchar(128) NOT NULL,
  `price` float NOT NULL,
  `quantity` int NOT NULL,
  `artist_id` int NOT NULL,
  `genre_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_art_artist` (`artist_id`),
  KEY `fk_artwork__genre` (`genre_id`),
  CONSTRAINT `fk_art_artist` FOREIGN KEY (`artist_id`) REFERENCES `artists` (`id`),
  CONSTRAINT `fk_artwork__genre` FOREIGN KEY (`genre_id`) REFERENCES `genres` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `artworks`
--

LOCK TABLES `artworks` WRITE;
/*!40000 ALTER TABLE `artworks` DISABLE KEYS */;
INSERT INTO `artworks` VALUES (1,'All eyes on you','artwork','all-eyes-on-you',1200,5,1,1),(2,'Bomb Head','artwork','bomb-head',850,5,1,2),(3,'Child Soldier','artwork','child-soldier',900,10,2,3),(4,'Clacton on Sea','artwork','clacton-on-sea',1200,5,1,1),(5,'Colourful women','artwork','colourful-woman',1000,2,3,5),(6,'Dismaland','artwork','dismaland',600,12,4,4),(7,'Girl with balloon','artwork','girl-with-balloon',1500,1,2,1),(8,'Girl with Rose','artwork','girl-with-rose',860,6,3,4),(9,'Golden Future','artwork','golden-future',930,9,2,2),(10,'HOPE','artwork','hope-obama',400,20,4,3),(11,'Hung Lover','artwork','hung-lover',300,15,3,5),(12,'Olympic','artwork','olympic-rings',650,5,1,1),(13,'Cat','artwork','pink-cat',210,20,4,2),(14,'Queen','artwork','posters-queen',1000,5,4,2),(15,'Power','artwork','power-equality',600,12,3,1),(16,'Pulp Fiction','artwork','pulp-fiction-banana',800,6,4,4),(17,'Remove','artwork','removal-art-prints',1350,2,3,3),(18,'City','artwork','urban-city',250,12,3,1);
/*!40000 ALTER TABLE `artworks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `canvases`
--

DROP TABLE IF EXISTS `canvases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `canvases` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `image` varchar(128) NOT NULL,
  `length` int NOT NULL,
  `width` int NOT NULL,
  `price` float NOT NULL,
  `quantity` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `canvases`
--

LOCK TABLES `canvases` WRITE;
/*!40000 ALTER TABLE `canvases` DISABLE KEYS */;
/*!40000 ALTER TABLE `canvases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cart_items`
--

DROP TABLE IF EXISTS `cart_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cart_items` (
  `id` int NOT NULL AUTO_INCREMENT,
  `item_name` varchar(128) NOT NULL,
  `quantity` int NOT NULL,
  `cart_id` int NOT NULL,
  `price_per_one` float NOT NULL,
  `price` float NOT NULL,
  `item_category` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cart_item__cart` (`cart_id`),
  CONSTRAINT `fk_cart_item__cart` FOREIGN KEY (`cart_id`) REFERENCES `carts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart_items`
--

LOCK TABLES `cart_items` WRITE;
/*!40000 ALTER TABLE `cart_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `cart_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `carts`
--

DROP TABLE IF EXISTS `carts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `carts` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carts`
--

LOCK TABLES `carts` WRITE;
/*!40000 ALTER TABLE `carts` DISABLE KEYS */;
INSERT INTO `carts` VALUES (1,1);
/*!40000 ALTER TABLE `carts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flyway_schema_history`
--

DROP TABLE IF EXISTS `flyway_schema_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flyway_schema_history` (
  `installed_rank` int NOT NULL,
  `version` varchar(50) DEFAULT NULL,
  `description` varchar(200) NOT NULL,
  `type` varchar(20) NOT NULL,
  `script` varchar(1000) NOT NULL,
  `checksum` int DEFAULT NULL,
  `installed_by` varchar(100) NOT NULL,
  `installed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `execution_time` int NOT NULL,
  `success` tinyint(1) NOT NULL,
  PRIMARY KEY (`installed_rank`),
  KEY `flyway_schema_history_s_idx` (`success`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flyway_schema_history`
--

LOCK TABLES `flyway_schema_history` WRITE;
/*!40000 ALTER TABLE `flyway_schema_history` DISABLE KEYS */;
INSERT INTO `flyway_schema_history` VALUES (1,'1100','artists and artworks tables','SQL','V1100__artists_and_artworks_tables.sql',-1279148398,'root','2020-05-17 19:04:51',238,1),(2,'1101','tools and paints tables','SQL','V1101__tools_and_paints_tables.sql',-952611450,'root','2020-05-17 19:04:51',518,1),(3,'1102','add and fill genres table','SQL','V1102__add_and_fill_genres_table.sql',-2041051916,'root','2020-05-17 19:04:51',149,1),(4,'1103','create canvas table','SQL','V1103__create_canvas_table.sql',-449223602,'root','2020-05-17 19:04:52',42,1),(5,'1104','add users and invoice table','SQL','V1104__add_users_and_invoice_table.sql',938241029,'root','2020-05-17 19:04:52',162,1),(6,'1105','fill artists and arts table','SQL','V1105__fill_artists_and_arts_table.sql',82911336,'root','2020-05-17 19:04:52',8,1),(7,'1106','fill tool and paint types tables','SQL','V1106__fill_tool_and_paint_types_tables.sql',-97408985,'root','2020-05-17 19:04:52',9,1),(8,'1107','entity tables modifying','SQL','V1107__entity_tables_modifying.sql',-1366967058,'root','2020-05-17 19:04:52',278,1),(9,'1108','create cart tables','SQL','V1108__create_cart_tables.sql',1114572688,'root','2020-05-17 19:04:52',137,1),(10,'1109','create invoice item table','SQL','V1109__create_invoice_item_table.sql',-3266802,'root','2020-05-17 19:04:52',50,1),(11,'1110','create resets table','SQL','V1110__create_resets_table.sql',290282431,'root','2020-05-17 19:04:52',43,1),(12,'1111','create review table','SQL','V1111__create_review_table.sql',1015876757,'root','2020-05-17 19:04:52',44,1);
/*!40000 ALTER TABLE `flyway_schema_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genres`
--

DROP TABLE IF EXISTS `genres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `genres` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genres`
--

LOCK TABLES `genres` WRITE;
/*!40000 ALTER TABLE `genres` DISABLE KEYS */;
INSERT INTO `genres` VALUES (1,'Graffiti','Graffiti can provide either a secure message or sometimes give some jazzy view. Many times, they are made from very bright colors to fascinate everyone who passes by.'),(2,'Poster Art','Posters are the specific type of printed papers with some 2D artistic drawing, painting or calligraphic elements on it.'),(3,'Sticker Art','This art uses images with stickers to post them publicly. They promote political, entertainment or society related things. '),(4,'Bombing','bombers cover statues, signs, trees, grocery carts, benches and other objects with bright threads'),(5,'Wildstyle','Elaborate, interlocking letters or symbols used when tagging. Wildstyle forms a complicated code that excludes non-writers');
/*!40000 ALTER TABLE `genres` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice_items`
--

DROP TABLE IF EXISTS `invoice_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `invoice_items` (
  `id` int NOT NULL AUTO_INCREMENT,
  `item_id` int NOT NULL,
  `item_name` varchar(128) NOT NULL,
  `quantity` int NOT NULL,
  `item_category` varchar(128) NOT NULL,
  `price_per_one` float NOT NULL,
  `price` float NOT NULL,
  `invoice_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_invoice_item_invoice` (`invoice_id`),
  CONSTRAINT `fk_invoice_item_invoice` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice_items`
--

LOCK TABLES `invoice_items` WRITE;
/*!40000 ALTER TABLE `invoice_items` DISABLE KEYS */;
INSERT INTO `invoice_items` VALUES (1,1,'All eyes on you',1,'artwork',1200,1200,1),(2,2,'Bomb Head',1,'artwork',850,850,2);
/*!40000 ALTER TABLE `invoice_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoices`
--

DROP TABLE IF EXISTS `invoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `invoices` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `phone_number` varchar(128) NOT NULL,
  `address` varchar(128) NOT NULL,
  `sum_price` float NOT NULL,
  `invoice_date` date NOT NULL,
  `invoice_time` time NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_invoice__user` (`user_id`),
  CONSTRAINT `fk_invoice__user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoices`
--

LOCK TABLES `invoices` WRITE;
/*!40000 ALTER TABLE `invoices` DISABLE KEYS */;
INSERT INTO `invoices` VALUES (1,1,'0703 54 31 88','Chernyshevskogo 33',1200,'2020-05-17','19:05:56'),(2,1,'0703 54 31 88','Chernyshevskogo 33',850,'2020-05-17','19:13:16');
/*!40000 ALTER TABLE `invoices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paint_types`
--

DROP TABLE IF EXISTS `paint_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `paint_types` (
  `id` int NOT NULL AUTO_INCREMENT,
  `type_of_paint` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paint_types`
--

LOCK TABLES `paint_types` WRITE;
/*!40000 ALTER TABLE `paint_types` DISABLE KEYS */;
INSERT INTO `paint_types` VALUES (1,'acrylic'),(2,'oil'),(3,'spray'),(4,'watercolor');
/*!40000 ALTER TABLE `paint_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paints`
--

DROP TABLE IF EXISTS `paints`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `paints` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `category` varchar(128) NOT NULL DEFAULT 'paint',
  `volume` int NOT NULL,
  `quantity` int NOT NULL,
  `description` varchar(128) NOT NULL,
  `image` varchar(128) NOT NULL,
  `price` float NOT NULL,
  `paint_type_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_paint__paint_type` (`paint_type_id`),
  CONSTRAINT `fk_paint__paint_type` FOREIGN KEY (`paint_type_id`) REFERENCES `paint_types` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paints`
--

LOCK TABLES `paints` WRITE;
/*!40000 ALTER TABLE `paints` DISABLE KEYS */;
/*!40000 ALTER TABLE `paints` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resets`
--

DROP TABLE IF EXISTS `resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `resets` (
  `id` int NOT NULL AUTO_INCREMENT,
  `token` varchar(128) NOT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_reset__user_id` (`user_id`),
  CONSTRAINT `fk_reset__user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resets`
--

LOCK TABLES `resets` WRITE;
/*!40000 ALTER TABLE `resets` DISABLE KEYS */;
INSERT INTO `resets` VALUES (1,'28b9b9c7-4eac-4ecc-8781-4ba4f7ee4362',1);
/*!40000 ALTER TABLE `resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reviews` (
  `id` int NOT NULL AUTO_INCREMENT,
  `review_body` varchar(128) NOT NULL,
  `artwork_id` int NOT NULL,
  `user_id` int NOT NULL,
  `user_login` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews`
--

LOCK TABLES `reviews` WRITE;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
INSERT INTO `reviews` VALUES (1,'Good work',2,1,'sana'),(2,'Perfect',2,1,'sana');
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tool_types`
--

DROP TABLE IF EXISTS `tool_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tool_types` (
  `id` int NOT NULL AUTO_INCREMENT,
  `type_of_tool` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tool_types`
--

LOCK TABLES `tool_types` WRITE;
/*!40000 ALTER TABLE `tool_types` DISABLE KEYS */;
INSERT INTO `tool_types` VALUES (1,'brush'),(2,'liner'),(3,'pencil'),(4,'marker'),(5,'caps');
/*!40000 ALTER TABLE `tool_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tools`
--

DROP TABLE IF EXISTS `tools`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tools` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `category` varchar(128) NOT NULL DEFAULT 'tool',
  `description` varchar(128) NOT NULL,
  `quantity` int NOT NULL,
  `image` varchar(128) NOT NULL,
  `price` float NOT NULL,
  `tool_type_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tool__tool_type` (`tool_type_id`),
  CONSTRAINT `fk_tool__tool_type` FOREIGN KEY (`tool_type_id`) REFERENCES `tool_types` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tools`
--

LOCK TABLES `tools` WRITE;
/*!40000 ALTER TABLE `tools` DISABLE KEYS */;
/*!40000 ALTER TABLE `tools` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `surname` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `login` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `role` varchar(128) NOT NULL DEFAULT 'USER',
  `enabled` int NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Sanat','Kasymov','sanatkasymov1997@gmail.com','sana','$2a$10$sKssmcuKZucqVDOkoOiLueANH.wCteCiMDrLZkIHHNU81hkGaWStm','USER',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-18  1:27:30
