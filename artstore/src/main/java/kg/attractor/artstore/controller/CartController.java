package kg.attractor.artstore.controller;
import kg.attractor.artstore.model.CartItem;
import kg.attractor.artstore.model.Invoice.Invoice;
import kg.attractor.artstore.repository.ArtworkRepository;
import kg.attractor.artstore.service.CartService;
import kg.attractor.artstore.service.SessionService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.List;


@Controller
@AllArgsConstructor
public class CartController {
    private final CartService cartService;
    private final SessionService sessionService;

    @GetMapping("/cart")
    public String getCurrentUserId(Model model){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userEmail = auth.getName();
        if(!userEmail.equals("anonymousUser")){
            List<CartItem> cartItemList = cartService.getThisUserCart(userEmail);
            if (cartItemList != null) {
                model.addAttribute("cartItems", cartItemList);
            }
        }
        return "cart";
    }

    @PostMapping("/cart/add")
    public String addToCart(@RequestParam Integer id, String category, int quantity, HttpSession session){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userEmail = auth.getName();
        cartService.addNewItem(id, category, quantity, userEmail);
        //хранение в сессии
        sessionService.addItemToCart(id, session);

        return "redirect:/cart";
    }

    @PostMapping("/cart/remove")
    public String removeItem(@RequestParam Integer itemId, HttpSession session){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userEmail = auth.getName();
        cartService.removeThisItem(itemId, userEmail);

        //удаление отдельных элементов из корзины в сессии
        sessionService.removeItem(itemId, session);

        return "redirect:/cart";
    }

    @GetMapping("/order")
    public String getOrderPage(Model model){
        return "order";
    }


    @PostMapping("/order")
    public String order(@RequestParam String customerPhoneNumber, String customerAddress, Model model, HttpSession session){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userEmail = auth.getName();
        Invoice invoice = cartService.createNewOrder(userEmail,customerPhoneNumber, customerAddress);
        model.addAttribute("invoice", invoice);

        //очищение корзины в сессии
        sessionService.removeCart(session);

        return "success";
   }

    @GetMapping("/success")
    public String getSuccessfulOrderPage(Model model){
        return "success";
    }
}


