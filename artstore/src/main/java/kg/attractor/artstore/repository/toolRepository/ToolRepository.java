package kg.attractor.artstore.repository.toolRepository;

import kg.attractor.artstore.model.Tool.Tool;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ToolRepository extends JpaRepository<Tool, Integer> {
}
