package kg.attractor.artstore.DTO;
import kg.attractor.artstore.model.Invoice.Invoice;
import kg.attractor.artstore.model.User;
import lombok.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class InvoiceDTO {
    private Integer id;
    private User user;
    private String phoneNumber;
    private float sumPrice;
    private LocalDate invoiceDate;
    private LocalTime invoiceTime;

    static InvoiceDTO from(Invoice invoice){
        return builder()
                .id(invoice.getId())
                .user(invoice.getUser())
                .phoneNumber(invoice.getPhoneNumber())
                .sumPrice(invoice.getSumPrice())
                .invoiceDate(invoice.getInvoiceDate())
                .invoiceTime(invoice.getInvoiceTime())
                .build();
    }
}
