package kg.attractor.artstore.DTO;
import kg.attractor.artstore.model.Artist;
import lombok.*;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ArtistDTO {
    private Integer id;
    private String name;
    private String surname;
    private int age;
    private String description;
    private String photo;


    public static ArtistDTO from(Artist artist){
        return builder()
                .id(artist.getId())
                .name(artist.getName())
                .surname(artist.getSurname())
                .age(artist.getAge())
                .description(artist.getDescription())
                .photo(createPhotoPath(artist.getPhoto()))
                .build();
    }
    private static String createPhotoPath(String photoName){
        return String.format("/images/artists/%s.jpg", photoName);
    }
}
