package kg.attractor.artstore.DTO;
import kg.attractor.artstore.model.Paint.Paint;
import kg.attractor.artstore.model.Paint.PaintType;
import lombok.*;
import java.util.List;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PaintDTO {
    private Integer id;
    private String name;
    private int volume;
    private String description;
    private int quantity;
    private String image;
    private float price;
    private PaintTypeDTO paintType;

    static PaintDTO from(Paint paint){
        return builder()
                .id(paint.getId())
                .name(paint.getName())
                .volume(paint.getVolume())
                .description(paint.getDescription())
                .quantity(paint.getQuantity())
                .image(paint.getImage())
                .price(paint.getPrice())
                .paintType(PaintTypeDTO.from(paint.getPaintType()))
                .build();
    }

    @Data
    @Builder(access = AccessLevel.PRIVATE)
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class PaintTypeDTO {
        private int id;
        private String typeOfPaint;
        private List<Paint> paints;

        public static PaintTypeDTO from(PaintType paintType) {
            return builder()
                    .id(paintType.getId())
                    .typeOfPaint(paintType.getTypeOfPaint())
                    .paints(paintType.getPaints())
                    .build();
        }
    }
}
