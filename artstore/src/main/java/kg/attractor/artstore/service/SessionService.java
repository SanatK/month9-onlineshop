package kg.attractor.artstore.service;

import kg.attractor.artstore.model.Artwork;
import kg.attractor.artstore.repository.ArtworkRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class SessionService {
    private final ArtworkRepository artworkRepository;

    public void addItemToCart(Integer id, HttpSession session){
        Artwork artwork = artworkRepository.findAllById(id);
        if (session != null) {
            var attr = session.getAttribute(Constants.CART_ID);
            if (attr == null) {
                session.setAttribute(Constants.CART_ID, new ArrayList<Artwork>());
            }
            try {
                var list = (List<Artwork>) session.getAttribute(Constants.CART_ID);
                list.add(artwork);
            } catch (ClassCastException ignored) {

            }
        }
    }

    public String removeItem(Integer itemId, HttpSession session){
        if (session != null) {
            var attr = session.getAttribute(Constants.CART_ID);
            if (attr == null) {
                return "redirect:/cart";
            }
            try {
                var list = (List<Artwork>) session.getAttribute(Constants.CART_ID);
                for(int i = 0; i<list.size(); i++){
                    if(list.get(i).getId()==itemId){
                        list.remove(i);
                    }
                }
            } catch (ClassCastException ignored) {

            }
        }
        return "success";
    }

    public void removeCart(HttpSession session){
        session.removeAttribute(Constants.CART_ID);
    }

}
