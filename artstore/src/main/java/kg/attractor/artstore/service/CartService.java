package kg.attractor.artstore.service;

import kg.attractor.artstore.exception.ProductNotFoundException;
import kg.attractor.artstore.model.Cart;
import kg.attractor.artstore.model.CartItem;
import kg.attractor.artstore.model.Invoice.Invoice;
import kg.attractor.artstore.model.Invoice.InvoiceItem;
import kg.attractor.artstore.model.User;
import kg.attractor.artstore.repository.ArtworkRepository;
import kg.attractor.artstore.repository.CartItemRepository;
import kg.attractor.artstore.repository.CartRepository;
import kg.attractor.artstore.repository.UserRepository;
import kg.attractor.artstore.repository.invoiceRepository.InvoiceItemRepository;
import kg.attractor.artstore.repository.invoiceRepository.InvoiceRepository;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class CartService {
    private final UserRepository userRepository;
    private final CartRepository cartRepository;
    private final CartItemRepository cartItemRepository;
    private final ArtworkRepository artworkRepository;
    private final InvoiceRepository invoiceRepository;
    private final InvoiceItemRepository invoiceItemRepository;



    public List<CartItem> getThisUserCart(String userEmail){
        Integer userId = userRepository.findUserByEmail(userEmail).getId();
        Cart cart = cartRepository.findByUserId(userId);
        List<CartItem> cartItems = cartItemRepository.findAllByCart_Id(cart.getId());
        return cartItems;
    }

    public String addNewItem(Integer id, String category, int quantity, String userEmail){
        Integer userId = userRepository.findUserByEmail(userEmail).getId();
        Cart cart = cartRepository.findByUserId(userId);
        if(cart==null){
            cart = cartRepository.save(new Cart(userId));
        }
        cartItemRepository.save(createCartItem(id, category, quantity, cart));
        return "success";
    }

    public void removeThisItem(Integer itemId, String userEmail){
        Integer userId = userRepository.findUserByEmail(userEmail).getId();
        Cart cart = cartRepository.findByUserId(userId);
        List<CartItem> cartItems = cartItemRepository.findAllByCart_Id(cart.getId());
        for(CartItem c: cartItems){
            if(c.getId().equals(itemId)){
                cartItemRepository.deleteById(itemId);
            }
        }
    }

    public Invoice createNewOrder(String userEmail, String phoneNumber, String address){
        User user = userRepository.findUserByEmail(userEmail);
        Cart cart = cartRepository.findByUserId(user.getId());
        List<CartItem> cartItems = cartItemRepository.findAllByCart_Id(cart.getId());
        float sumPrice = getSumPrice(cartItems);
        Invoice invoice = createNewInvoice(user, phoneNumber, address, sumPrice);
        invoiceRepository.save(invoice);
        createInvoiceAndDeleteCartItems(cartItems, invoice);
        return invoice;

    }

    public CartItem createCartItem(Integer id, String category, int quantity, Cart cart){
        if(category.equals("artwork")){
            var artwork = artworkRepository.findAllById(id);
            return CartItem.builder()
                    .itemName(artwork.getName())
                    .quantity(quantity)
                    .pricePerOne(artwork.getPrice())
                    .price(artwork.getPrice()*quantity)
                    .itemCategory(category)
                    .cart(cart)
                    .build();
        }else if(category.equals("paint")){
            //TODO
        }else if(category.equals("tool")){
            //TODO
        }
        throw new ProductNotFoundException();
    }


    public float getSumPrice(List<CartItem> invoices){
        float sum = 0;
        for(CartItem item: invoices){
            sum += item.getPrice();
        }
        return sum;
    }

    public Invoice createNewInvoice(User user, String phoneNumber, String address, float price){
        return  Invoice.builder()
                .user(user)
                .phoneNumber(phoneNumber)
                .address(address)
                .sumPrice(price)
                .invoiceDate(LocalDate.now())
                .invoiceTime(LocalTime.now())
                .build();
    }

    public void createInvoiceAndDeleteCartItems(List<CartItem> cartItems, Invoice invoice){
        List<InvoiceItem> invoiceItems = new ArrayList<>();
        for(CartItem c : cartItems){
            invoiceItems.add(InvoiceItem.builder()
                            .itemId(c.getId())
                            .itemName(c.getItemName())
                            .quantity(c.getQuantity())
                            .itemCategory(c.getItemCategory())
                            .pricePerOne(c.getPricePerOne())
                            .price(c.getPrice())
                            .invoice(invoice)
                            .build());
            cartItemRepository.deleteById(c.getId());
        }
        invoiceItemRepository.saveAll(invoiceItems);
    }
}
