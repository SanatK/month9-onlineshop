package kg.attractor.artstore.service;

import kg.attractor.artstore.DTO.ArtworkDTO;
import kg.attractor.artstore.exception.ResourceNotFoundException;
import kg.attractor.artstore.model.Artwork;
import kg.attractor.artstore.repository.ArtworkRepository;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class ArtworkService {
    private final ArtworkRepository artworkRepository;

    public Page<ArtworkDTO> getArtworks(Pageable pageable){
        return artworkRepository.findAll(pageable)
                .map(ArtworkDTO::from);
    }

    public ArtworkDTO getArtwork(int id){
        var artwork = artworkRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("place", id));
        return ArtworkDTO.from(artwork);
    }

    public List<ArtworkDTO> findArtworks(String textToSearch){
        List<Artwork> artworks = artworkRepository.findAll();
        List<ArtworkDTO> artworksDTO = new ArrayList<>();
        for(Artwork a: artworks){
            if(a.getName().toLowerCase().contains(textToSearch.toLowerCase())){
                artworksDTO.add(ArtworkDTO.from(a));
            }
        }
        return artworksDTO;
    }
}
