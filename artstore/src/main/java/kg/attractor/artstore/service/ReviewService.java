package kg.attractor.artstore.service;

import kg.attractor.artstore.model.Invoice.Invoice;
import kg.attractor.artstore.model.Invoice.InvoiceItem;
import kg.attractor.artstore.model.Review;
import kg.attractor.artstore.repository.ArtworkRepository;
import kg.attractor.artstore.repository.ReviewRepository;
import kg.attractor.artstore.repository.UserRepository;
import kg.attractor.artstore.repository.invoiceRepository.InvoiceItemRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ReviewService {
    private final InvoiceItemRepository invoiceItemRepository;
    private final ArtworkRepository artworkRepository;
    private final UserRepository userRepository;
    private final ReviewRepository reviewRepository;

    public boolean isUserBuyThisProduct(List<Invoice> invoice, String category, Integer id){
        if(invoice!=null){
            return checkUserItemLists(invoice, category, id);
        }
        return false;
    }

    public boolean checkUserItemLists(List<Invoice> invoice, String category, Integer id){
       for(int i = 0; i<invoice.size(); i++){
            List<InvoiceItem> userItems = invoiceItemRepository.findAllByInvoice_Id(invoice.get(i).getId());
            if(userItems!=null){
                if(checkUserItems(userItems, category, id)){
                    return true;
                }
            }
        }
       return false;
    }

    public boolean checkUserItems(List<InvoiceItem> userItems, String category, Integer id){
        for(int z =0; z<userItems.size(); z++){
            if(userItems.get(z).getItemCategory().equals(category)&&userItems.get(z).getItemName().equals(artworkRepository.findAllById(id).getName())){
                return true;
            }
        }
        return false;
    }

    public void addNewReview(Integer id, String category, String review, String userEmail){
        Integer thisUserId = userRepository.findByEmail(userEmail).get().getId();
        String userLogin = userRepository.findUserByEmail(userEmail).getLogin();
        Review newReview = Review.builder()
                .reviewBody(review)
                .userId(thisUserId)
                .artworkId(id)
                .userLogin(userLogin)
                .build();
        reviewRepository.save(newReview);
    }
}
