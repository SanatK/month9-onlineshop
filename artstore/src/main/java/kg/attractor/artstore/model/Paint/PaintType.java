package kg.attractor.artstore.model.Paint;
import kg.attractor.artstore.model.Artwork;
import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@Table(name = "paint_types")
@Entity
public class PaintType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank
    @Size(min = 1, max = 128)
    @Column(length = 128)
    private String typeOfPaint;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "paintType")
    @OrderBy("name ASC")
    private List<Paint> paints;
}


