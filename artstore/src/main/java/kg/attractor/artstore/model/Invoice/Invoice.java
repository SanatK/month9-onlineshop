package kg.attractor.artstore.model.Invoice;
import kg.attractor.artstore.model.Paint.PaintType;
import kg.attractor.artstore.model.User;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@Data
@Entity
@Table(name = "invoices")
public class Invoice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @NotBlank
    @Size(min=1, max=128)
    @Column(length = 128)
    private String phoneNumber;

    @NotBlank
    @Size(min=1, max=128)
    @Column(length = 128)
    private String address;

    @NotNull
    @Column
    private float sumPrice;

    @Column
    private LocalDate invoiceDate;

    @Column
    private LocalTime invoiceTime;

}
