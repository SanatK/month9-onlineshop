package kg.attractor.artstore.model;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor
@Data
@Entity
@Table(name = "cart_items")
public class CartItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 128)
    @NotBlank
    @Size(min = 1, max = 128)
    private String itemName;

    @NotNull
    @Column
    private Integer quantity;

    @NotNull
    @Column
    private float pricePerOne;

    @NotNull
    @Column
    private float price;


    @Column(length = 128)
    @NotBlank
    @Size(min = 1, max = 128)
    private String itemCategory;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cart_id")
    private Cart cart;
}
