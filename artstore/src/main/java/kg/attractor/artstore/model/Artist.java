package kg.attractor.artstore.model;
import lombok.*;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data @Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Entity
@Table(name = "artists")
public class Artist {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank
    @Size(min = 1, max = 128)
    @Column(length = 128)
    private String name;

    @NotBlank
    @Size(min = 1, max = 128)
    @Column(length = 128)
    private String surname;

    @NotNull
    @Column
    private int age;

    @NotBlank
    @Size(min = 1, max = 128)
    @Column(length = 128)
    private String description;

    @NotBlank
    @Size(min = 1, max = 128)
    @Column(length = 128)
    private String photo;
}
