package kg.attractor.artstore.model.Tool;
import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@Table(name = "tool_types")
@Entity
public class ToolType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank
    @Size(min = 1, max = 128)
    @Column(length = 128)
    private String typeOfTool;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "toolType")
    @OrderBy("name ASC")
    private List<Tool> tools;
}

