package kg.attractor.artstore.model.Paint;
import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Entity
@Table(name = "paints")
public class Paint {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank
    @Size(min = 1, max = 128)
    @Column(length = 128)
    private String name;

    @Column
    private int volume;

    @NotBlank
    @Size(min = 1, max = 128)
    @Column(length = 128)
    private static final String CATEGORY = "paint";

    @NotBlank
    @Size(min = 1, max = 128)
    @Column(length = 128)
    private String description;

    @NotNull
    @Column
    private int quantity;

    @NotBlank
    @Size(min = 1, max = 128)
    @Column(length = 128)
    private String image;

    @NotNull
    @Column
    private float price;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "paint_type_id")
    private PaintType paintType;
}
