use `artstore`;

insert into `tool_types` (`type_of_tool`) values
('brush'), ('liner'), ('pencil'), ('marker'), ('caps');

insert into `paint_types` (`type_of_paint`) values
('acrylic'), ('oil'), ('spray'), ('watercolor');