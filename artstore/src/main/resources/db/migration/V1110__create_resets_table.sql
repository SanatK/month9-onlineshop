use `artstore`;


CREATE TABLE `resets` (
`id` INT auto_increment NOT NULL,
`token` varchar(128) NOT NULL,
`user_id` INT NOT NULL,
PRIMARY KEY(`id`),
CONSTRAINT `fk_reset__user_id`
FOREIGN KEY (`user_id`)
REFERENCES `users` (`id`)
);
