use `artstore`;

CREATE TABLE `tools` (
 `id` INT auto_increment NOT NULL,
 `name` varchar(128) NOT NULL,
 `description` varchar(128) NOT NULL,
 `quantity` int not null,
 `image` varchar(128) NOT NULL,
 `price` float not null,
 PRIMARY KEY (`id`)
);
create table `tool_types` (
 `id` INT auto_increment NOT NULL,
 `type_of_tool` varchar(128) NOT NULL,
 PRIMARY KEY (`id`)
);
alter table `tools`
 add column `tool_type_id` INT NOT NULL after `price`,
 add CONSTRAINT `fk_tool__tool_type`
 FOREIGN KEY (`tool_type_id`)
 REFERENCES `tool_types` (`id`);

CREATE TABLE `paints` (
 `id` INT auto_increment NOT NULL,
 `name` varchar(128) NOT NULL,
 `volume` int not null,
 `quantity` int not null,
 `description` varchar(128) NOT NULL,
 `image` varchar(128) NOT NULL,
 `price` float not null,
 PRIMARY KEY (`id`)
);
create table `paint_types` (
 `id` INT auto_increment NOT NULL,
 `type_of_paint` varchar(128) NOT NULL,
 PRIMARY KEY (`id`)
);
alter table `paints`
 add column `paint_type_id` INT NOT NULL after `price`,
 add CONSTRAINT `fk_paint__paint_type`
 FOREIGN KEY (`paint_type_id`)
 REFERENCES `paint_types` (`id`);

