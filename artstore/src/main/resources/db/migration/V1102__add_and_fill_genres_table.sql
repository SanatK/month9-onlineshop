use `artstore`;
create table `genres` (
 `id` INT auto_increment NOT NULL,
 `name` varchar(128) NOT NULL,
 `description` varchar(255) NOT NULL,
 PRIMARY KEY (`id`)
);
alter table `artworks`
 add column `genre_id` INT NOT NULL after `artist_id`,
 add CONSTRAINT `fk_artwork__genre`
 FOREIGN KEY (`genre_id`)
 REFERENCES `genres` (`id`);
insert into `genres` (`name`, `description`) values
 ('Graffiti', 'Graffiti can provide either a secure message or sometimes give some jazzy view. Many times, they are made from very bright colors to fascinate everyone who passes by.'),
 ('Poster Art', 'Posters are the specific type of printed papers with some 2D artistic drawing, painting or calligraphic elements on it.'),
 ('Sticker Art', 'This art uses images with stickers to post them publicly. They promote political, entertainment or society related things. '),
 ('Bombing', 'bombers cover statues, signs, trees, grocery carts, benches and other objects with bright threads'),
 ('Wildstyle', 'Elaborate, interlocking letters or symbols used when tagging. Wildstyle forms a complicated code that excludes non-writers');
