use `artstore`;

CREATE TABLE `invoice_items` (
`id` INT auto_increment NOT NULL,
`item_id` INT NOT NULL,
`item_name`varchar(128) NOT NULL,
`quantity` INT NOT NULL,
`item_category` varchar(128) NOT NULL,
`price_per_one` float NOT NULL,
`price` float NOT NULL,
`invoice_id` INT NOT NULL,
PRIMARY KEY(`id`),
CONSTRAINT `fk_invoice_item_invoice`
FOREIGN KEY (`invoice_id`)
REFERENCES `invoices` (`id`)
);
