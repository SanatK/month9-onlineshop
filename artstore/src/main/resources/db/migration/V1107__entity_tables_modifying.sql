use `artstore`;

alter table `users`
 add column `role` varchar(128) NOT NULL default 'USER' after `password`,
 add column `enabled` INT NOT NULL  default true after `role`;

alter table `artworks`
 add column `category` varchar(128) NOT NULL default 'artwork' after `name`;

alter table `tools`
 add column `category` varchar(128) NOT NULL default 'tool' after `name`;

alter table `paints`
 add column `category` varchar(128) NOT NULL default 'paint' after `name`;